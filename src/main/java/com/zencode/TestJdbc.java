package com.zencode;

import com.zencode.data.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class TestJdbc {
    public static void main(String[] args) {
            SessionFactory factory = new Configuration()
                    .configure()
                    .addAnnotatedClass(Student.class)
                    .buildSessionFactory();

            try (Session session = factory.getCurrentSession();) {
                // start transaction
                session.beginTransaction();

                // save the student
                session.save(new Student("Mruno", "Bars", "Brian@zencode.co.za"));
                session.save(new Student("Jin", "Reno", "Mike@zencode.co.za"));
                session.save(new Student("Mike", "Pines", "Brian@zencode.co.za"));

                // commit the transaction
                session.getTransaction().commit();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                factory.close();
            }
    }
}
