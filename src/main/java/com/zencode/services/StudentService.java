package com.zencode.services;

import com.zencode.data.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;


public class StudentService implements AutoCloseable {
    SessionFactory factory;

    public StudentService() {
        factory = new Configuration()
                .configure()
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();
    }

    public void addStudent(Student student) {
        try(Session session = factory.getCurrentSession()) {
            session.beginTransaction();

            session.save(student);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateFirstName(int id, String name) {
        try (Session session = factory.getCurrentSession()) {
            session.beginTransaction();
            Student student = session.get(Student.class, id);

            student.setFirstName(name);

            session.getTransaction().commit();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void updateLastName(int id, String name) {
        try (Session session = factory.getCurrentSession()) {
            session.beginTransaction();
            Student student = session.get(Student.class, id);

            student.setLastName(name);

            System.out.println(student);

            session.getTransaction().commit();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteStudent(int id) {
        try (Session session = factory.getCurrentSession()) {
            session.beginTransaction();

            Student student = session.get(Student.class, id);

            if (student != null) {
                session.delete(student);
            }

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateEmail(int id, String email) {
        try (Session session = factory.getCurrentSession()) {
            session.beginTransaction();
            Student student = session.get(Student.class, id);

            student.setEmail(email);

            System.out.println(student);

            session.getTransaction().commit();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public Student getStudent(int id) {
        Student student = null;

        try (Session session = factory.getCurrentSession()) {
            session.beginTransaction();

            student = session.get(Student.class, id);

            session.beginTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return student;
    }

    public List<Student> getStudents() {
        List<Student> students;

        try (Session session = factory.getCurrentSession()) {
            session.beginTransaction();

            students = session
                    .createQuery("from Student order by firstName")
                    .getResultList();

            session.getTransaction().commit();
        }

        return students;
    }

    @Override
    public void close() throws Exception {
        factory.close();
    }
}
