package com.zencode;

import com.zencode.data.Student;
import com.zencode.services.StudentService;

public class Client {
    public static void main(String[] args) {
        try (StudentService service = new StudentService();) {
            service.updateFirstName(3, "Chuck");
            service.deleteStudent(4);

            service.getStudents().forEach(System.out::println);
            service.addStudent(new Student("Sulk", "Hmash", "smash@zencode.co.za"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
